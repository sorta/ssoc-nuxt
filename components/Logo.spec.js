import { shallowMount } from '@vue/test-utils'
import Logo from './Logo.vue'

describe('Logo', () => {
  test('is a Vue instance', () => {
    const wrapper = shallowMount(Logo)
    expect(wrapper.isVueInstance()).toBeTruthy()
  })

  test('renders correctly', () => {
    const wrapper = shallowMount(Logo)
    expect(wrapper.element).toMatchSnapshot()
  })
})
