import { mount } from '@vue/test-utils'
import index from './index.vue'
import Logo from '@/components/Logo.vue'

describe('Logo', () => {
  test('Has a Logo component', () => {
    const page = mount(index)
    expect(page.find(Logo).exists()).toBe(true)
  })

  test('renders correctly', () => {
    const page = mount(index)
    expect(page.element).toMatchSnapshot()
  })
})
