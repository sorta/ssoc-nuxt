export const state = () => ({
  list: [
    {
      slug: 'travis-ci',
      title: 'Travis CI'
    },
    {
      slug: 'avibe',
      title: 'A-Vibe'
    },
    {
      slug: 'sorta',
      title: 'Personal'
    }
  ]
})
